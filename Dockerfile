FROM python:3-alpine
RUN apk add --update --no-cache build-base libffi-dev
RUN apk add --update --no-cache openssl-dev
WORKDIR /dogebot
ADD ./ /dogebot/
RUN pip3 install -r requirements.txt
 
