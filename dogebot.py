# dogebot - Copyright (C) 2019 by judopandatrader
# Released under GPL v2. See LICENSE.md
# https://twitter.com/judopandatrader

from binance.client import Client
from binance.websockets import BinanceSocketManager
from twisted.internet import reactor

from os import environ

from decimal import Decimal, ROUND_DOWN

BUY_SPOTS = (
    '0.00000015',
    '0.00000019',
    '0.00000024',
    '0.00000030',
    '0.00000035',
    '0.00000039',
)

SELL_SPOTS = (
    '0.00000060',
    '0.00000071',
    '0.00000081',
    '0.00000090',
    '0.00000101',
    '0.00000110',
    '0.00000120',
    '0.00000131',
    '0.00000141',
    '0.00000150',
)

CURRENCY_PAIR = 'DOGEBTC'
LEFT_SYMBOL = 'DOGE'
RIGHT_SYMBOL = 'BTC'
MIN_ORDER_SIZE = '0.001'

API_KEY = environ['BINANCE_KEY']
API_SECRET = environ['BINANCE_SECRET']

################################################

from pprint import pprint
from time import sleep

eights = lambda x: '%.8f' % x
twos = lambda x: '%.2f' % x

# Compute Decimal arrays for math
_buy_spots = sorted(list(map(lambda x: Decimal(x), BUY_SPOTS)), reverse=True)
_sell_spots = sorted(list(map(lambda x: Decimal(x), SELL_SPOTS)))
_min_order = Decimal(MIN_ORDER_SIZE)

client = Client(API_KEY, API_SECRET)

print("Cancelling all orders...")

current_orders = client.get_open_orders(symbol=CURRENCY_PAIR)
for c in current_orders:
    client.cancel_order(symbol=c['symbol'], orderId=str(c['orderId']))

# Fetch the current action
_current_btc_balance = Decimal('0.00000000')
_current_doge_balance = Decimal('0.00000000')


any_locked = True

print("Waiting for all assets to unlock...")

while any_locked:
    any_locked = False
    account = client.get_account()
    for symbol in account['balances']:
        if Decimal(symbol['locked']) > Decimal('0.00000000'):
            any_locked = True
            sleep(1)

for symbol in account['balances']:
    if symbol['asset'] == 'BTC':
        _current_btc_balance = Decimal(symbol['free'])
    if symbol['asset'] == 'DOGE':
        _current_doge_balance = Decimal(symbol['free'])


print("BTC Balance: {}".format(eights(_current_btc_balance)))
print("DOGE Balance: {}".format(eights(_current_doge_balance)))


_current_price = Decimal('0.00000000')
tickers = client.get_all_tickers()
for ticker in tickers:
    if ticker['symbol'] == CURRENCY_PAIR:
        _current_price = Decimal(ticker['price'])

while _buy_spots[0] > _current_price:
    _buy_spots.pop(0)

while _sell_spots[0] < _current_price:
    _sell_spots.pop(0)


def compute_buy_orders():
    global _buy_spots, _current_btc_balance, _min_order

    while True:
        if len(_buy_spots) == 0:
            return []
        orders = []
        per_order = _current_btc_balance / Decimal(len(_buy_spots))

        for b in _buy_spots:
            doge_count = (per_order / b).quantize(Decimal('0'), rounding=ROUND_DOWN)
            orders.append((doge_count, b, doge_count * b, ))

        below_minimum = False
        for o in orders:
            if o[2] < _min_order:
                below_minimum = True

        if below_minimum is False:
            break
        else:
            _buy_spots.pop()

    if below_minimum is True:
        return []

    return orders


def compute_sell_orders():
    global _sell_spots, _current_doge_balance, _min_order

    orders = []
    per_order = _current_doge_balance / Decimal(len(_sell_spots))

    while True:
        if len(_sell_spots) == 0:
            return []
        orders = []
        per_order = _current_doge_balance / Decimal(len(_sell_spots))

        for s in _sell_spots:
            doge_count = per_order
            orders.append((doge_count.quantize(Decimal('0'), rounding=ROUND_DOWN), s, doge_count * s ))

        below_minimum = False
        for o in orders:
            if o[2] < _min_order:
                below_minimum = True

        if below_minimum is False:
            break
        else:
            _sell_spots.pop()

    if below_minimum is True:
        return []

    return orders


print("Reconciling account size with minimum order size ({})".format(MIN_ORDER_SIZE))
print("Trimming list of BUY spots accordingly.")
buy_orders = compute_buy_orders()
print("Trimming list of SELL spots accordingly.")
sell_orders = compute_sell_orders()

print("Result: {} BUY spots, and {} SELL spots.".format(len(buy_orders), len(sell_orders)))

if len(buy_orders) > 0:
    print("\n### Placing Buy Orders ###")
    for o in buy_orders:
        current_order = client.create_order(
            symbol=CURRENCY_PAIR,
            side=client.SIDE_BUY,
            type=client.ORDER_TYPE_LIMIT,
            timeInForce=client.TIME_IN_FORCE_GTC,
            quantity=eights(o[0]),
            price=eights(o[1])
        )
        print("Order #{}, Quantity {}, DOGE, Price {} BTC, Value {} BTC".format(current_order['orderId'], twos(o[0]), eights(o[1]), eights(o[2])))

if len(sell_orders) > 0:
    print("\n### Placing Sell Orders ###")
    for o in sell_orders:
        current_order = client.create_order(
            symbol=CURRENCY_PAIR,
            side=client.SIDE_SELL,
            type=client.ORDER_TYPE_LIMIT,
            timeInForce=client.TIME_IN_FORCE_GTC,
            quantity=eights(o[0]),
            price=eights(o[1])
        )
        print("Order #{}, Quantity {}, DOGE, Price {} BTC, Value {} BTC".format(current_order['orderId'], twos(o[0]), eights(o[1]), eights(o[2])))
