# dogebot

## Summary

dogebot will automate the placement of buy/sell orders on the DOGEBTC pair on Binance.

It is based on the notion that DOGEBTC always booms and busts.

This is alpha software. Use it at your own risk. You should use a dedicated Binance account for this, as the bot doesn't care about your other orders/assets right now and will go hog wild.


## How it works

1. You define a series of buy/sell prices in dogebot.py
2. Set `BINANCE_KEY` and `BINANCE_SECRET` env variables.
3. Run dogebot
4. Dogebot will get your BTC and DOGE assets, and cancel all existing orders.
5. Dogebot will look at your buy/sell ladders. It will ignore buys above the current price, and sells below the current price. It will then determine whether, if accomodating all your desired price levels, you'd meet the minimum order size on Binance. If not, it will start trimming your buy and sell ladders (lowest offer, highest offer, respectively), until the minimum order sizes are satisfied.
6. Dogebot will place new orders and show you what they are.
7. Rinse cycle and repeat every couple of weeks.


## Build and Run

Docker is the recommended installation method. You can build the code with Docker as such:

```
docker build --pull -ti dogebot:latest .
```

You can then run it with the following command:

```
docker run --rm -e BINANCE_KEY=XXX -e BINANCE_SECRET=XXX -ti dogebot:latest python3 dogebot.py
```

Obviously, you'll want to put valid values for `BINANCE_KEY` and `BINANCE_SECRET`, or manage it as you normally manage your Docker containers.


## License

GPL v2 - see `LICENSE.md`

